import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.image.Image;

public class TrianguloControlador {

    @FXML
    private Button cmdActualizarMedidas;

    @FXML
    private Button cmdDeterminarTipo;

    @FXML
    private Button btnAarea;

    @FXML
    private Button btnPerimetro;

    @FXML
    private Label txtRespuesta;

    @FXML
    private ImageView imgTriangulo;

    @FXML
    private TextField txtX1;

    @FXML
    private TextField txtY1;

    @FXML
    private TextField txtX2;

    @FXML
    private TextField txtY2;

    @FXML
    private TextField txtX3;

    @FXML
    private TextField txtY3;
    
    private Triangulo triangulo;
    
    public TrianguloControlador() {
        triangulo = new Triangulo();
    }

    @FXML
    void actualizarMedidas() {
        double x1 = Double.parseDouble(txtX1.getText());
        double y1 = Double.parseDouble(txtY1.getText());
        triangulo.setPunto1(x1, y1);
        
        double x2 = Double.parseDouble(txtX2.getText());
        double y2 = Double.parseDouble(txtY2.getText());
        triangulo.setPunto2(x2, y2);
        
        double x3 = Double.parseDouble(txtX3.getText());
        double y3 = Double.parseDouble(txtY3.getText());
        triangulo.setPunto3(x3, y3);
    }

    @FXML
    void determinarTipo() {
        String tipo = "Desconocido";
        if (triangulo.esEquilatero()) {
            tipo = "Equilatero";
        } else {
            if (triangulo.esIsoseles()) {
                tipo = "Isosceles";
            } else if (triangulo.esEscaleno()) {
                tipo = "Escaleno";
            }
            if (triangulo.esRectangulo()) {
                tipo = tipo + " Rectángulo";
            }
        }//fin else
        imgTriangulo.setImage(new Image ("img/"+tipo+".png"));
        txtRespuesta.setText(tipo);
    }
    
    @FXML
    void obtenerArea() {
        double area = triangulo.getArea();
        String strDouble = String.format("%.2f", area);
        txtRespuesta.setText("Área: " + strDouble);
    }

    @FXML
    void obtenerPerimetro() {
        double perimetro = triangulo.getPerimetro();
        String strDouble = String.format("%.2f", perimetro);
        txtRespuesta.setText("Perímetro: " + strDouble);
    }

}
