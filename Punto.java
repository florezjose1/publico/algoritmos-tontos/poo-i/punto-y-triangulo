/**
 * Write a description of class Punto here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */

import java.text.DecimalFormat;

public class Punto {

    protected double x;
    protected double y;
    
    public Punto() {
    }
    
    public Punto(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return this.x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return this.y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double calcularDistancia(Punto otroPunto) {
        double x = Math.pow((this.getX() - otroPunto.getX()), 2);
        double y = Math.pow((this.getY() - otroPunto.getY()), 2);
        // Acortamos decimales parar que en el equilátero los valores no sean tan precisos...
        String strDouble = String.format("%.1f", Math.sqrt(x+y));
        // reemplazamos la coma por el punto para poderse convertir a double
        strDouble = strDouble.replace(",", ".");
        return Double.parseDouble(strDouble);
    }
}