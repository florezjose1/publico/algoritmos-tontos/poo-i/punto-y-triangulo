
/**
 * Write a description of class Triangulo here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class Triangulo {
    
    protected Punto punto1; 
    protected Punto punto2; 
    protected Punto punto3;
    
    public double getArea() {
        double l1 = this.punto1.calcularDistancia(this.punto2);
        double l2 = this.punto2.calcularDistancia(this.punto3);
        double l3 = this.punto3.calcularDistancia(this.punto1);
        double s = this.getPerimetro() / 2;  // Semiperímetro
        return Math.sqrt(s * (s-l1) * (s-l2) * (s-l3));  // Formula de Herón
    }

    public double getPerimetro() {
        return this.punto1.calcularDistancia(this.punto2) + 
               this.punto2.calcularDistancia(this.punto3) + 
               this.punto3.calcularDistancia(this.punto1);
    }

    public boolean esEquilatero() {
        double l1 = this.punto1.calcularDistancia(this.punto2);
        double l2 = this.punto2.calcularDistancia(this.punto3);
        double l3 = this.punto3.calcularDistancia(this.punto1);
        System.out.println("l1: " + l1);
        System.out.println("l2: " + l2);
        System.out.println("l3: " + l3);
        return l1 == l2 && l2 == l3;
    }

    public boolean esEscaleno() {
        double l1 = this.punto1.calcularDistancia(this.punto2);
        double l2 = this.punto2.calcularDistancia(this.punto3);
        double l3 = this.punto3.calcularDistancia(this.punto1);
        return l1 != l2 && l2 != l3 && l1 != l3;
    }

    public boolean esIsoseles() {
        double l1 = this.punto1.calcularDistancia(this.punto2);
        double l2 = this.punto2.calcularDistancia(this.punto3);
        double l3 = this.punto3.calcularDistancia(this.punto1);
        return l1 == l2 && l2 != l3 || 
               l1 == l3 && l3 != l2 || 
               l2 == l3 && l3 != l1;
    }

    public boolean esRectangulo() {
        double l1 = this.punto1.calcularDistancia(this.punto2);
        double l2 = this.punto2.calcularDistancia(this.punto3);
        double l3 = this.punto3.calcularDistancia(this.punto1);
        
        return l1 + l2 > l3 || 
               l2 + l3 > l1 || 
               l1 + l3 > l2;
    }

    public Triangulo(double x1, double y1, double x2, double y2, double x3, double y3) {
        this.punto1 = new Punto(x1, y1);
        this.punto2 = new Punto(x2, y2);
        this.punto3 = new Punto(x3, y3);
    }

    public Triangulo(Punto punto1, Punto punto2, Punto punto3) {
        this.punto1 = punto1;
        this.punto2 = punto2;
        this.punto3 = punto3;
    }
    
    public Triangulo() {
        this.punto1 = new Punto();
        this.punto2 = new Punto();
        this.punto3 = new Punto();
    }
    
    public void setPunto1(double x, double y) {
        this.punto1.setX(x);
        this.punto1.setY(y);
    }
    
    public void setPunto2(double x, double y) {
        this.punto2.setX(x);
        this.punto2.setY(y);
    }
    
    public void setPunto3(double x, double y) {
        this.punto3.setX(x);
        this.punto3.setY(y);
    }
}